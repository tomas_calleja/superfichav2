import { Component } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { AlertController } from '@ionic/angular';

// Map the character names to their Sheet IDs
const characterIDs = {
  'ireena': '1IIQd0JKOYhYlDWnoFP72EdZ29vjVemvqoeo9qWqQNQk',
  'veloren': '1U0XolFPaFccQabMSBovSpJRiPc2KeNSckoOhgFdbKuA',
  'tak': '1YiA0H19sEK4lROpt-vhK78GCLPkl_y6UHD1QLRgygKc',
  'raz': '1veRxgdM1CQwXKYNKjfvmjwpyV4CHpZXdldtmXhAlPpg',
  'durmont': '1xNw05Pj-JM5VvptTFlJa7qXOv4fn8aFxFIWQnDTw0Vc',
  'ana': '1ThAM2T9W-qiHMg-IT_UI88HJWjyIxRnoi_qmJs7lr4g',
  'belrin': '1YgHDT5bNuoORoFvuygzPTBCdkub34n72vq3gcmwQz_s'
}

const default_character = 'belrin';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
    ngOnInit(){
      console.log("cargando")
      const characterName = this.route.snapshot.queryParamMap.get('characterName')
      this.sheetId = characterIDs[characterName] 
      if (! this.sheetId) { 
        this.sheetId = this.route.snapshot.queryParamMap.get('sheet');
        if (!this.sheetId) {
          console.log("Usuario missing, usando hardcoded " + default_character)
          this.sheetId = characterIDs[default_character];
        }
      } 
      this.loadAll();
    }
    sheetId: string
    data: object;
    charData: any;
    loaded: number;
    general: boolean;
    caracteristicas: boolean;
    presentacion: boolean;
    tab: number;
    section: number;
    spell: boolean;
    info: boolean;
    adv: boolean;
    disadv: boolean;
    bless: boolean;
    rolling: boolean;
    ini: boolean;
    ataquesData: object[];
    ataquesDataFilter: any;
    habilidadesData: any;
    habilidadesDataFilter: any;
    conjurosData: object[];
    iniciativasData: object[];
    conjurosDataFilter: any;
    equipData: object[];
    equipDataFilter: any;
    diceFav: boolean;
    diceSkill: boolean;
    diceAttack: boolean;
    diceDamage: boolean;
    diceTS: boolean;
    diceMenu: boolean;
    allOptions: boolean;
    iconsMap: object;
    constructor(private http: HttpClient, private route: ActivatedRoute, public alertController: AlertController) {
      this.data = {};
      this.charData = {};
      this.loaded = 0;
      this.caracteristicas = false;
      this.general = false;
      this.presentacion = false;
      this.tab = 1;
      this.section = 3;
      this.spell = false;
      this.info = true;
      this.adv = false;
      this.disadv = false;
      this.bless = false;
      this.rolling = false;
      this.ini = true;
      this.ataquesData = [];
      this.ataquesDataFilter = [];
      this.habilidadesData = [];
      this.habilidadesDataFilter = [];
      this.conjurosData = [];
      this.iniciativasData = [];
      this.conjurosDataFilter = [];
      this.equipData = [];
      this.equipDataFilter = [];
      this.diceFav = false;
      this.diceSkill = false;
      this.diceAttack = false;
      this.diceDamage = false;
      this.diceMenu = false;
      this.diceTS = false;
      this.allOptions = true;
      this.iconsMap = nameToSVG;
    }
    loadAll(){
      this.loaded=0;
      this.ataquesData = [];
      this.ataquesDataFilter = [];
      this.habilidadesData = [];
      this.habilidadesDataFilter = [];
      this.conjurosData = [];
      this.conjurosDataFilter = [];
      this.equipData = [];
      this.equipDataFilter = [];
      // Load the data for the different tabs
      this.getDataInfo();
      this.getDataCombat();
      this.getDataDices();
      this.getDataInventory();
      this.getDataMagic();
      this.getDataIniciativa();
    }
    /* setHeaders returns the headers to get JSON data (from https://spreadsheets.google.com by default) and allowing credentials */
    private setHeaders(origin : string = "https://spreadsheets.google.com") {
      let headers = new HttpHeaders();
      headers.append("Accept", 'application/json');
      headers.append('Content-Type', 'application/json');
      headers.append("Access-Control-Allow-Origin", origin);
      headers.append('Access-Control-Allow-Credentials', "true");
      return headers;
    }
    /* getDataInfo obtains the character info data from the first tab of the spreadsheet (INFO)*/
    getDataInfo() {
      let headers = this.setHeaders();
      let url = `https://spreadsheets.google.com/feeds/cells/${this.sheetId}/1/public/values?alt=json`
      this.http.get(url, {headers: headers})
          .subscribe(data => {
              this.loaded = this.loaded+1;
              console.log("raw",data);
              let tempCharData = {};
              for (let index = 0; index < data['feed']['entry'].length; index++) {
                const element = data['feed']['entry'][index]['gs$cell'];
                if(element.col === "1"){
                  tempCharData[element.row] = {prop: element['$t']}
                }else if(element.col !== "1"){
                  tempCharData[element.row][`value_${element.col}`] = element['$t']
                }
              }
              for (const key in tempCharData) {
                if (tempCharData.hasOwnProperty(key)) {
                  const element = tempCharData[key];
                  this.charData[element.prop] = element;
                }
              }
              console.log("datos limpios", this.charData)
          }, error => {
              console.log(error);
          });
    }
    /* getDataCombat obtains the combat data from the sixth tab of the spreadsheet (COMBAT)*/
    getDataCombat() {
      let headers = this.setHeaders();
      let url = `https://spreadsheets.google.com/feeds/cells/${this.sheetId}/6/public/values?alt=json`
  
      this.http.get(url, {headers: headers})
          .subscribe(data => {
            this.loaded = this.loaded+1;
              //console.log(data);
              this.ataquesData = this.spreadsheetToObjectArray(data);
              console.log("datos limpios", this.ataquesData);
              this.ataquesDataFilter = this.ataquesData;
          }, error => {
              console.log(error);
          });
    }
    /* getDataDices obtains the skills data from the second tab of the spreadsheet (SKILLS)*/
    getDataDices() {
      let headers = this.setHeaders();
      let url = `https://spreadsheets.google.com/feeds/cells/${this.sheetId}/2/public/values?alt=json`
      
      this.http.get(url, {headers: headers})
      .subscribe(data => {
        this.loaded = this.loaded+1;
        console.log(data);
        let tempCharData = {};
        for (let index = 1; index < data['feed']['entry'].length; index++) {
          const element = data['feed']['entry'][index]['gs$cell'];
          if(element.col === "1"){
            tempCharData[element.row] = {prop: element['$t']}
          }else if(element.col !== "1"){
            if(tempCharData[element.row]){
              tempCharData[element.row][`value_${element.col}`] = element['$t']
            }
          }
        }
        for (const key in tempCharData) {
          if (tempCharData.hasOwnProperty(key)) {
            const element = tempCharData[key];
            // TODO: if we pass the functions onr enhance the objects later we can use the spreadsheetToList function to process the data
            element.cssClass = this.getCssClass(element);
            element.badges = this.getBadges(element);
            this.habilidadesData.push(element);
          }
        }
        console.log("datos limpios", this.habilidadesData)
        this.habilidadesDataFilter = this.habilidadesData;
      }, error => {
        console.log(error);
      });
    }
    /* getDataInventory obtains the inventory data from the third tab of the spreadsheet (EQUIP)*/
    getDataInventory() {
      let headers = this.setHeaders();
      let url = `https://spreadsheets.google.com/feeds/cells/${this.sheetId}/3/public/values?alt=json`
  
      this.http.get(url, {headers: headers})
          .subscribe(data => {
            this.loaded = this.loaded+1;
              //console.log(data);
              let equipHeaders = {
                "prop": "ID",
                "value_2": "Peso",
                "value_3": "Unidades",
                "value_4": "Objeto"
              };
              // TODO: I'm getting an error here on the Visual Studio Code, but it seems its working.
              this.equipData = this.spreadsheetToObjectArray(data);
              this.equipData.unshift(equipHeaders);
              console.log("datos limpios", this.equipData)
              this.equipDataFilter = this.equipData;
          }, error => {
              console.log(error);
          });
    }
    /* getDataMagic obtains the magic data from the fifth tab of the spreadsheet (MAGIA)*/
    getDataMagic() {
      let headers = this.setHeaders();
      let  url = `https://spreadsheets.google.com/feeds/cells/${this.sheetId}/5/public/values?alt=json`

      this.http.get(url, {headers: headers})
          .subscribe(data => {
              this.loaded = this.loaded+1;
              this.conjurosData = this.spreadsheetToObjectArray(data);
              console.log("datos limpios", this.conjurosData)
              this.conjurosDataFilter = this.conjurosData;
          }, error => {
              console.log(error);
          });
    }
    reset(){
      this.ataquesDataFilter = this.ataquesData;
      this.conjurosDataFilter = this.conjurosData;
      this.equipDataFilter = this.equipData;
      this.habilidadesDataFilter = this.habilidadesData;
      
    }
    getConjuros(ev: any) {
      // Reset items back to all of the items
      this.conjurosDataFilter = this.conjurosData;
      // set val to the value of the searchbar
      const val = ev.target.value;
      // if the value is an empty string don't filter the items
      if (val && val.trim() != '') {
        this.conjurosDataFilter = this.conjurosDataFilter.filter((item) => {
          return (item.prop.toLowerCase().indexOf(val.toLowerCase()) > -1);
        })
      }
    }
    getEquipo(ev: any) {
      // Reset items back to all of the items
      this.equipDataFilter = this.equipData;
      // set val to the value of the searchbar
      const val = ev.target.value;
      // if the value is an empty string don't filter the items
      if (val && val.trim() != '') {
        this.equipDataFilter = this.equipDataFilter.filter((item) => {
          return (item.value_4.toLowerCase().indexOf(val.toLowerCase()) > -1);
        })
      }
    }
    getHabilidades(ev: any) {
      // Reset items back to all of the items
      this.habilidadesDataFilter = this.habilidadesData;
      // set val to the value of the searchbar
      const val = ev.target.value;
      // if the value is an empty string don't filter the items
      if (val && val.trim() != '') {
        this.habilidadesDataFilter = this.habilidadesDataFilter.filter((item) => {
          if(item.value_2){
            return (item.value_2.toLowerCase().indexOf(val.toLowerCase()) > -1);
          }else{
            return false
          }
        })
      }
    }
    async rollDice(description, dicepattern, dicepatternv, dicepatternd) {
      let headers = this.setHeaders("https://pasarelav2-mzdpemxmza-ew.a.run.app");
      const pjName = this.charData.nombre.value_2 || "Donh Joe";
      let url = "https://pasarelav2-mzdpemxmza-ew.a.run.app"
      let myDicePatern = dicepattern;
      if(this.adv){
        myDicePatern = dicepatternv;
      }else if(this.disadv){
        myDicePatern = dicepatternd;
      }
      let myObj = {
          "pj": pjName,
          "description": description,
          "dicepattern": myDicePatern,
          "reroll1": false,
          "d4": this.bless,
          "adv": this.adv,
          "disadv": this.disadv
      };
      this.rolling = true;
      this.http.post(url, myObj, {headers: headers})
          .subscribe(async data => {
              console.log("datos get", data)
              let alertCssClass = 'my-custom-class';
              if (data['critico'] === 1 ) {
                alertCssClass = 'my-custom-class critico';
              }
              if (data['critico'] === 2 ) {
                alertCssClass = 'my-custom-class pifia';
              }
              const alert = await this.alertController.create({
                cssClass: alertCssClass,
                header: pjName,
                subHeader: description + ': ' + data['result'],
                message: 'Resultado: '+ data['dices'],
                buttons: ['OK']
              });
              this.adv = false;
              this.disadv = false;
              this.bless = false;
              this.rolling = false;
              this.diceMenu = false;
              await alert.present();
          }, error => {
              this.rolling = false;
              console.log(error);
          });
    }
    getDataIniciativa() {
      this.iniciativasData = [];
      let headers = this.setHeaders();
      //la misma hoja para todos
      let url = `https://spreadsheets.google.com/feeds/cells/1MiuzFnGuUpSwSsGt5ph9qWOXa4Ig_LmB_FMI_dpe9z4/2/public/values?alt=json`
      this.http.get(url, {headers: headers})
      .subscribe(data => {
        this.iniciativasData = this.spreadsheetToObjectArray(data);
    }, error => {
        console.log(error);
    });
    }
  /**
   * spreadsheetToObjectArray receives as input a JSON object as received from Google Spreadsheets and converts it
   * to an Array of objects with the data cells. 
   * It asumes the first row it's the headers of the table an ignores it.
   * @param data Google spreadsheets data
   * @returns an array of objects from the data cells of the spreadsheet
   */
  private spreadsheetToObjectArray(data: Object) : Object[] {
    let tempCharData = {};
    for (let index = 0; index < data['feed']['entry'].length; index++) {
      const element = data['feed']['entry'][index]['gs$cell'];
      // Ignore the first row as it contains the titles
      if (element.row !== "1") {
        if (element.col === "1") {
          tempCharData[element.row] = { prop: element['$t'] };
        }
        else {
          tempCharData[element.row][`value_${element.col}`] = element['$t'];
        }
      }
    }
    let result = [];
    for (const key in tempCharData) {
      if (tempCharData.hasOwnProperty(key)) {
        const element = tempCharData[key];
       result.push(element);
      }
    }
    return result;
  }

    //haz métodos como Dios manda, que esto no es twitter XD
    // Es por si hay que cambiar el prefijo y se mueven a otro directorio (en plan assets/icons)
    addAssetsPrefix(s : string) : string { return 'assets/' + s}
    /**
     * getCSSClass receives a skill and adds the corresponding css class mostly depending on the 
     * value of the column 8 of the spreadsheet, but also if the name starts with Salvación
     * @param skill object with a property `value_8` with the skill category and a property `value_2`
     *  with the name of the skill
     * @returns string with the cssClass name corresponding to the Skill
     */
    getCssClass(skill) : string {
      var classes: string[] = [];
      if (skill.value_5) {
        classes = ["trained "];
      }
      switch (skill.value_8) {
        case 'COMBAT':
          classes.push('combat');
          break;
        case 'TS':
          if ((typeof skill.value_2) === 'string' && skill.value_2.startsWith('Salvación')) {
            classes.push('pure-ts');
            break;
          } 
          classes.push('skill-ts');
          break;
        case 'SKILL':
          classes.push('skill');
          break;
        case 'LANGUAGE':
          classes.push('language');
          break;
        case 'DAMAGE':
          classes.push('damage');
          break;
        default:
          console.log(skill.value_8);
          classes.push('unknown');
          break;
      }
      return classes.join(' ');
    } 
    /**
     * getBadges receives a skill as handled from the spreadsheet and adds the array of badges 
     * splitting the values from column 13
     * @param skill with a property `value_13` that contains a comma separated list of badge names
     * @returns string[] an array of strings corresponding to the badges or an empty array if there are no badges
     */
    getBadges(skill) : string[] {
      // console.log(skill.value_13);
      if (typeof skill.value_13 === 'string') {
        return skill.value_13.split(',');
      }
      return skill.value_13 || [];
    }
  }

/* TODO: Move this to it's own file */
/* nameToSVG it's a mapping from the SKILL column value on the spreadsheet to the related SVG icon */ 
const nameToSVG = {
  // Attack
  'At.Estoque': 'stiletto.svg',
  'At.Kukri': 'knife.svg',
  'At.Daga': 'plain-dagger.svg',
  'At.Daga Normal': 'plain-dagger.svg',
  'At.Ballesta M': 'crossbow.svg',
  'At.Ballesta P': 'crossbow.svg',
  'At.Ballesta': 'crossbow.svg',
  'At.Thorn Whip': 'whip.svg',
  'At.Vicious Mok': 'shouting.svg',
  'At.Dissonant W': 'screaming_2.svg',
  'At.Firebolt': 'firebolt.svg',
  'At.Fire Bolt': 'firebolt.svg',
  'At.Toque Vampírico': 'fangs.svg',
  'At.Scorching Ray': 'scorching_ray.svg',
  'At.Lanza 2M': 'spear_2_hands_v2.svg',
  'At.Lanza 1M': 'spear.svg',
  'ChillTouch': 'skeletal-hand.svg',
  'At.Arco': 'bow.svg',
  'At.Francotird': 'bow_sniper.svg',
  'At.Espadas': 'sword.svg',
  'At.Cuchillo': 'knife.svg',
  'At.Garras+1': 'claws.svg',
  'At.Bastón': 'wizard-staff.svg',
  'At.Burning hands': 'fire-spell-cast.svg',
  'At.Word of radiance': 'shouting.svg',
  'At.Arma Espiritual': 'spiritual_weapon.svg',
  'At.Guiding Bolt': 'ringed-beam.svg',
  'At.Sacred Flame': 'lightning-flame.svg',
  'At.Hacha Adam.': 'war_axe.svg',
  'At.Hacha Salvaje': 'battle-axe.svg',
  'At.Pistola': 'pistol.svg',
  'At.Hacha': 'battle-axe.svg',
  'At.Espada': 'sword.svg',
  'At.Daga plegab': 'butterfly-knife.svg',
  'At.Eldritch B.': 'ringed-beam.svg',
  // Damage 
  'BonFire': 'pyre.svg',
  'Cuchillos Plata': 'daggers2.svg',
  'Dissonant Whispers': 'shouting.svg',
  'Spiritual Weapon': 'spiritual_weapon.svg',
  'Healing Word': 'healing_word.svg',
  //'Bola Fuego': 
  // Skills
  'Acrobacias': 'acrobatic.svg',
  'Actuar': 'drama-masks.svg',
  'Atletismo': 'run.svg',
  'Bajos fondos': 'snatch.svg',
  'Comerciar': 'coins.svg',
  'Con. Arcano': 'enlightenment.svg',
  'Con. Científico': 'corked-tube.svg',
  'Con. Cultural': 'caveman.svg',
  'Con. Militar': 'heavy-helm.svg',
  'Con. Monstruos': 'vampire-dracula.svg',
  'Con. Religioso': 'heptagram.svg',
  'Fabr. Armeria   .': 'blacksmith.svg',
  'Fabr. Destileria   .': 'cellar-barrels.svg',
  'Fabr. Sastreria': 'sewing-string.svg',
  'Fabr. Cocinar': 'camp-cooking-pot.svg',
  'Manejar animales': 'donkey.svg',
  'Mecanismos': 'gears.svg',
  'Medicina': 'medical-pack.svg',
  'Percepción': 'semi-closed-eye.svg',
  'Persuasión': 'convince.svg',
  'Psicología': 'think.svg',
  'Sigilo': 'mute.svg',
  'Supervivencia': 'wolf-trap.svg',
  'Trucos de manos': 'magic-trick.svg',
  // Languages 
  'Común': 'discussion.svg',
  'Draconiano': 'spiked-dragon-head.svg',
  'Enano': 'dwarf-face.svg',
  'Gigante': 'giant.svg',
  'Infernal': 'oni.svg',
  'Vistani': 'crystal-ball.svg',
  'Elfo': 'woman-elf-face.svg',
  // Chaaracteristics
  'Fuerza': 'biceps.svg',
  'Constitución': 'muscular-torso.svg',
  'Sabiduría': 'wisdom.svg',
  'Destreza': 'juggler.svg',
  'Inteligencia': 'brain.svg',
  'Carisma': 'smitten.svg',
  // Saving throws 
  'Salvación Fuerza': 'biceps.svg',
  'Salvación Constitución': 'muscular-torso.svg',
  'Salvación Sabiduría': 'wisdom.svg',
  'Salvación Destreza': 'juggler.svg',
  'Salvación Inteligencia': 'brain.svg',
  'Salvación Carisma': 'smitten.svg',
  // Default
  'default': 'dice-twenty-faces-twenty-red.svg'
} 